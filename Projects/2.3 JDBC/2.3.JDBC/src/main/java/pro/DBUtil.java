package pro;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtil {

    private static Connection con;

    public DBUtil() throws SQLException {
        this.con = createConnection();
    }

    public static Connection createConnection() throws SQLException {
        if (con == null) {
            Properties properties = new Properties();
            try {
                FileInputStream fileInputStream = new FileInputStream("src/main/resources/db.properties");
                properties.load(fileInputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return DriverManager.getConnection(
                    properties.getProperty("db.url"),
                    properties.getProperty("db.user"),
                    properties.getProperty("db.password"));
        }

        return con;
    }
}
