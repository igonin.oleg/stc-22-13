package pro;

import java.sql.*;
import java.util.Scanner;

public class Main {

    public static final String FIND_ALL = "SELECT * FROM ";
    public static void main(String[] args) throws SQLException {
        //Statement statement = DBUtil.createConnection().createStatement();
        Connection connection = DBUtil.createConnection();
//        Statement statement = connection.createStatement();
//        Scanner scanner = new Scanner(System.in);
//        String nameTable = scanner.nextLine();
//        //Ожидаемый реузультат - SELECT * FROM car
//        //ХаЦкер - SELECT * FROM car; DROP TABLE car;
//        ResultSet resultSet = statement.executeQuery(FIND_ALL + nameTable);
//        resultSet.next();
//
//        while (resultSet.next()) {
//            System.out.println(resultSet.getString(2) + " " + resultSet.getString(3) + " " + resultSet.getString(4));
//        }

        Scanner scanner = new Scanner(System.in);
        String nameTable = scanner.nextLine();
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL + nameTable);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            System.out.println(resultSet.getString(2) + " " + resultSet.getString(3));
        }

    }
}
